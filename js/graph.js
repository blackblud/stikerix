var ctx = document.getElementById('myChart').getContext('2d');
var chart = new Chart(ctx, {
    // The type of chart we want to create
    type: 'line',

    // The data for our dataset
    data: {
        labels: ['Квітень', 'Травень', 'Червень', 'Липень', 'Серпень', 'Вересень', 'Жовтень', 'Листопад', 'Грудень', 'Січень', 'Лютий', 'Березень'],
        datasets: [{
            backgroundColor: '#fee449',
            borderColor: '#1f1f1f',
            data: [0,10,30,20,112,50,169,151,239,270,375,401]
        }]
    }, 

    // Configuration options go here
    options: {
      legend: {
          display: false
      },
      layout: {
          padding: {
                left: 5,
                right: 5,
                top: 0,
                bottom: 0
            }
      },
      animation: {
          easing: 'easeInOutSine'
      },
      title: {
        display: true,
        text: '2019 - 2020'
      },
      tooltips: {
        enabled: false
      }
    }
});
