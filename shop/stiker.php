<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <title>STIKERIX - Магазин Стікерів</title>
    <meta charset="utf-8">
    <meta name="keywords" content="Stickers, Стікери, Stikerix, Наклейки">
    <meta name="description" content="Замовте Свій Набір Стікерів Уже Сьогодні!">
    <link rel="shortcut icon" href="../img/favicon.png">
    <link href="https://fonts.googleapis.com/css?family=Roboto:450&display=swap" rel="stylesheet">
    <script src="https://kit.fontawesome.com/15a7651b37.js" crossorigin="anonymous"></script>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>


    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <link rel="stylesheet" href="../css/shop.css">


    <script>
      function showUser(str) {
        if (str=="") {
          document.getElementById("shoop").innerHTML="";
          return;
        }
        if (window.XMLHttpRequest) {
          // code for IE7+, Firefox, Chrome, Opera, Safari
          xmlhttp=new XMLHttpRequest();
        } else { // code for IE6, IE5
          xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange=function() {
          if (this.readyState==4 && this.status==200) {
            document.getElementById("shoop").innerHTML=this.responseText;
          }
        }
        xmlhttp.open("GET","getstik.php?q="+str,true);
        xmlhttp.send();
      }
</script>


  </head>
  <body>

  <button id="btn_theme" class="btn_theme_light" type="button" name="change_theme" onclick="changetheme ()"><i id="logo_theme" class="far fa-moon"></i></button>

    <script type="text/javascript" src="../js/script.js"></script>
<div class="wrapper">
  <header>
        <a href="https://beemo.space"><img class="logo" src="../img/favicon.png" alt="Logo"></a>
        <div class="navig">
          <a class="navig_text" id="navig_caregory_main" href="../shop/stiker.php" title="Каталог Стікерів">Каталог Стікерів<i class="fas fa-angle-down"></i></a>
            <div class="navig_caregory">
              <table class="table_cat">
                <tr>
                  <th><a href="../shop/stiker.php">Стікери</a></th>
                  <th><a href="../shop/stiker.php">Мульфільми</a></th>
                  <th><a href="../shop/stiker.php">Кіно</a></th>
                  <th><a href="../shop/stiker.php">Geek/IT</a></th>
                </tr>
                <tr>
                  <td><a href="../shop/stiker.php">Стікерпаки</a></td>
                  <td><a href="../shop/stiker.php">Rick and Morty</a></td>
                  <td><a href="../shop/stiker.php">Матриця</a></td>
                  <td><a href="../shop/stiker.php">GitHub</a></td>
                </tr>
                <tr>
                  <td><a href="../shop/stiker.php">Стікербуки</a></td>
                  <td><a href="../shop/stiker.php">Simpson's</a></td>
                  <td><a href="../shop/stiker.php">Бійцівський Клуб</a></td>
                  <td><a href="../shop/stiker.php">Games</a></td>
                </tr>
                <tr>
                  <td><a href="../shop/stiker.php">Великі Наклейки</a></td>
                  <td><a href="../shop/stiker.php">Gravity Falls</a></td>
                  <td><a href="../shop/stiker.php">Термінатор</a></td>
                  <td><a href="../shop/stiker.php">IT Цитати</a></td>
                </tr>
                <tr>
                  <td><a href="../shop/stiker.php">Для Нього</a></td>
                  <td><a href="../shop/stiker.php">Futurama</a></td>
                  <td><a href="../shop/stiker.php">Титанік</a></td>
                  <td><a href="../shop/stiker.php">Хештеги</a></td>
                </tr>
                <tr>
                  <td><a href="../shop/stiker.php">Для Неї</a></td>
                  <td><a href="../shop/stiker.php">Adventure Time</a></td>
                  <td><a href="../shop/stiker.php">Назад у Майбутнє</a></td>
                  <td><a href="../shop/stiker.php">Програмування</a></td>
                </tr>
              </table>
            </div>
          <a class="navig_text" href="../../shop/stiker.php" title="Стікерпаки">Стікерпаки</a>
          <a class="navig_text" href="../dostavka.html" title="Доставка і Оплата">Доставка і Оплата</a>
          <a class="navig_text" href="../index.html#iabout" title="Про Нас">Про Нас</a>
          <a class="navig_text" href="../index.html#icontact" title="Контакти">Контакти</a>
          <a class="navig_text" id="sign_in" href="../sign_in.html" title="Вхід">Вхід / Реєстрація <i class="fas fa-sign-in-alt"></i></a>
        </div>
      </header>
</div>
<div class="wrapper">
  <div class="content">
    <div class="panel" id="panel_scroll">

      <button id="reload_btn" type="button" name="change_theme">
        <i class="fas fa-sync-alt"></i>
      </button>



      <div class="stik_price">
        <div class="zagolovok">
          Ціна
        </div>
        <div class="stik_price_range">
          <p>
            <label for="amount">Price range:</label>
            <input type="text" id="amount" readonly style="border:0; background-color: #f7f7f7; font-weight:bold;">
          </p>

          <div id="slider-range"></div>
        </div>
      </div>

      <hr>

      <div class="stik_type">
        <div class="zagolovok">
          Тип Наклейки
        </div>
        <div class="stik_type_radio category">

          <input class="stik_a" id="stik" type="radio" name="type_stik" checked="checked" value="Стікери" onclick="window.location='/shop/stik.html';">
          <label class="stik_a" for = "../shop/stiker.php">Стікери</label><br>

          <input class="stik_a" id="stik_pack" type="radio" name="type_stik" value="Стікерпаки" onclick="window.location='/shop/stik_pack.html';">
          <label class="stik_a" for = "../shop/stiker_pack.html">Стікерпаки</label><br>

          <input class="stik_a" id="stik_book" type="radio" name="type_stik" value="Стікербуки" onclick="window.location='/shop/stik_book.html';">
          <label class="stik_a" for = "../shop/stiker_book.html">Стікербуки</label><br>

          <input class="stik_a" id="big_stik" type="radio" name="type_stik" value="Великі Наклейки" onclick="window.location='/shop/big_stik.html';">
          <label class="stik_a" for = "../shop/big_stiker.html">Великі Наклейки</label><br>
        </div>
      </div>

      <hr>

      <div class="stik_category">
        <div class="zagolovok">
          Категорія - Geek/IT
        </div>
        <div class="category">
          <input class="vid" id="cat_1"type="checkbox" name="" value="">
          <label for="cat_1">GitHub</label><br>

          <input class="vid" id="cat_2"type="checkbox" name="" value="">
          <label for="cat_2">Games</label><br>

          <input class="vid" id="cat_3"type="checkbox" name="" value="">
          <label for="cat_3">IT Цитати</label><br>

          <input class="vid" id="cat_4"type="checkbox" name="" value="">
          <label for="cat_4">Хештеги</label><br>

          <input class="vid" id="cat_5"type="checkbox" name="" value="">
          <label for="cat_5">Програмування</label><br>
        </div>
        <hr>
        <div class="zagolovok">
          Категорія - Кіно
        </div>
        <div class="category">
          <input class="vid" id="cat_6"type="checkbox" name="" value="">
          <label for="cat_6">Матриця</label><br>

          <input class="vid" id="cat_7"type="checkbox" name="" value="">
          <label for="cat_7">Бійційвський Клуб</label><br>

          <input class="vid" id="cat_8"type="checkbox" name="" value="">
          <label for="cat_8">Термінатор</label><br>

          <input class="vid" id="cat_9"type="checkbox" name="" value="">
          <label for="cat_9">Титанік</label><br>

          <input class="vid" id="cat_10"type="checkbox" name="" value="">
          <label for="cat_10">Назад у Майбутнє</label><br>
        </div>
        <hr>
        <div class="zagolovok">
          Категорія - Мульфільми
        </div>
        <div class="category">
          <input class="vid" id="cat_11"type="checkbox" name="" value="">
          <label for="cat_11">Rick and Morty</label><br>

          <input class="vid" id="cat_12"type="checkbox" name="" value="">
          <label for="cat_12">Simpson's</label><br>

          <input class="vid" id="cat_13"type="checkbox" name="" value="">
          <label for="cat_13">Gravity Falls</label><br>

          <input class="vid" id="cat_14"type="checkbox" name="" value="">
          <label for="cat_14">Futurama</label><br>

          <input class="vid" id="cat_15"type="checkbox" name="" value="">
          <label for="cat_15">Adventure Time</label><br>
        </div>
        <hr>
        <div class="zagolovok">
          Категорія - Музика
        </div>
        <div class="category">
          <input class="vid" id="cat_16"type="checkbox" name="" value="">
          <label for="cat_16">Rock</label><br>

          <div class="sub-category">
            <input class="vid" id="cat_16_1"type="checkbox" name="" value="">
            <label for="cat_16" for="cat_16_1">Punk-Rock</label><br>

            <input class="vid" id="cat_16_2"type="checkbox" name="" value="">
            <label for="cat_16">Hard-Rock</label><br>

            <input class="vid" id="cat_16_3"type="checkbox" name="" value="">
            <label for="cat_16">Metal</label><br>

            <input class="vid" id="cat_16_4"type="checkbox" name="" value="">
            <label for="cat_16">Sinty Punk</label><br>
          </div>

          <input class="vid" id="cat_17"type="checkbox" name="" value="">
          <label for="cat_17">Rap</label><br>

          <input class="vid" id="cat_18"type="checkbox" name="" value="">
          <label for="cat_18">Jazz</label><br>

          <input class="vid" id="cat_19"type="checkbox" name="" value="">
          <label for="cat_19">Electro</label><br>

          <input class="vid" id="cat_20"type="checkbox" name="" value="">
          <label for="cat_20">Deep House</label><br>
        </div>
        <hr>
        <div class="zagolovok">
          Категорія - Чорно-Білі
        </div>
        <div class="category">
          <input class="vid" id="cat_21"type="checkbox" name="" value="">
          <label for="cat_21">GitHub</label><br>

          <input class="vid" id="cat_22"type="checkbox" name="" value="">
          <label for="cat_22">Games</label><br>

          <input class="vid" class="vid" id="cat_23"type="checkbox" name="" value="">
          <label for="cat_23">IT Цитати</label><br>

          <input class="vid" class="vid" id="cat_24"type="checkbox" name="" value="">
          <label for="cat_24">Хештеги</label><br>

          <input class="vid" class="vid" id="cat_25"type="checkbox" name="" value="">
          <label for="cat_25">Програмування</label><br>
        </div>
      </div>

      <hr>

      <a href="#" class="myButton">Застосувати Фільтри</a>


    </div>
    <div class="shop">
      <div class="shop_config">

        <div class="config_title">
          <h1 class="shop_config_text_h1">Магазин Стікерів</h1>
          Показано 12 з 6729 результатів
        </div>
        <div class="config_select">
          <div class="select_text">
            Cортування:
          </div>
          <select class="catalog-sorting-select" name="orderby" onchange="showUser(this.value)">
            <option value="0" selected="selected">По популярності</option>
            <option value="1">Дата: спочатку старі</option>
            <option value="2">Дата: спочатку нові</option>
            <option value="3">Ціна: спочатку дешеві</option>
            <option value="4">Ціна: спочатку дорожчі</option>
          </select>
        </div>
      </div>
      <div class="shop_content">
        <div class="shop_content_x" id="shoop">

<?php
  $conn = mysqli_connect("localhost", "root", "", "stiker_shop");

  if ($conn -> connect_eroor) {
    die("Could not connect: ". $conn -> connect_eroor);
  }

  $sql = "SELECT * FROM stik";
  $result = $conn-> query($sql);

  if ($result-> num_rows > 0){
    while ($row = $result-> fetch_assoc()) {
      echo "
      <div class=\"stik_block\">
        <div class=\"stik_block_img\">
          <img class=\"stik_img\" src=\"../tov/". $row["img"] ."\">
        </div>
        <div class=\"stik_block_title\">
          <div class=\"s_name\">
            <span>". $row["name"] ."</span>
            <p class=\"s_id\">id - [". $row["id"] ."]</p>
          </div>
          <div class=\"s_price\">
            <span >". $row["price"] ."</span>
            <span class=\"s_val\">₴</span>
            <span class=\"s_roz\"><i class=\"fas fa-expand-arrows-alt\"></i>6 см</span>
          </div>
        </div>
      </div>";
    }
  }
  else {
    echo "Fail Error 0";
  }
  $conn-> close();
?>

        </div>
      </div>
    </div>


  </div>
</div>
<div class="back_panel_down"></div>
<div class="wrapper">
    <div class="footer_back">
      <div class="foot_text">
        © Stikerix 1369 - 2020
      </div>
      <div class="social">
        <a class="btn" href="https://www.facebook.com/">
          <i class="fab fa-facebook"></i>
        </a>
        <a class="btn" href="https://twitter.com/">
          <i class="fab fa-twitter"></i>
        </a>
        <a class="btn" href="https://www.google.com/">
          <i class="fab fa-google"></i>
        </a>
        <a class="btn" href="https://www.instagram.com/">
          <i class="fab fa-instagram"></i>
        </a>
        <a class="btn" href="https://telegram.org/">
          <i class="fab fa-telegram"></i>
        </a>
      </div>
      <div class="foot_rights">
        All Rights Reserved
      </div>
    </div>
  </div>





  <script>
  $( function() {
  $( "#slider-range" ).slider({
    range: true,
    min: 0,
    max: 100,
    values: [25, 75 ],
    slide: function( event, ui ) {
      $( "#amount" ).val( "$" + ui.values[ 0 ] + " - $" + ui.values[ 1 ] );
    }
  });
  $( "#amount" ).val( "₴" + $( "#slider-range" ).slider( "values", 0 ) +
    " - ₴" + $( "#slider-range" ).slider( "values", 1 ) );
  } );
  </script>
  <script src="../js/jq_file.js"></script>


  </body>
</html>
